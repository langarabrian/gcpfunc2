import React, { Component } from 'react';

class Books extends Component {

  addBook(ev) {
    const inputISBN = ev.target.querySelector('[id="isbn"]');
    const isbn = parseInt( inputISBN.value.trim() );

    const inputTitle = ev.target.querySelector('[id="title"]');
    const title = inputTitle.value.trim();

    const inputPages = ev.target.querySelector('[id="pages"]');
    const pages = parseInt( inputPages.value.trim() );

    console.log( "ISBN: " + isbn );
    console.log( "Title: " + title );
    console.log( "Pages: " + pages );

    ev.preventDefault();
  }

  render() {
    return(
    <div>
      <div className="py-5 text-center">
        <h2>Books</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addBook.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="isbn">ISBN</label>
                <input type="number" className="form-control" id="isbn" defaultValue="1" required />
                <div className="invalid-feedback">
                    An ISBN is required.
                </div>
              </div>

              <div className="col-md-8 mb-3">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title" defaultValue="" required />
                <div className="invalid-feedback">
                    A book title is required.
                </div>
              </div>

              <div className="col-md-2 mb-3">
                <label htmlFor="pages">Pages</label>
                <input type="number" className="form-control" id="pages" defaultValue="100" required />
                <div className="invalid-feedback">
                    The number of pages is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add book</button>
          </form>
        </div>
      </div>

      <table className="table">
        <thead>
          <tr>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Pages</th>
          </tr>
        </thead>
        <tbody>

        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Books;
