// bring in Feathers API
const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');

// bring in application specific elements
const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./channels');
const mongoose = require('./mongoose');

// construct Feathers app
const app = feathers();

// Load app configuration
app.configure(configuration());

// Load mongoose configuration
app.configure(mongoose);

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);

// Set up our services (see `services/index.js`)
app.configure(services);

// Set up event channels (see channels.js)
app.configure(channels);

// register application level hooks
app.hooks(appHooks);

// Set up Google Cloud Functions entry point
exports.feathers = async (req, res) => {
  console.log( "method: " + req.method );
  console.log( "path: " + req.path );
  res.send('Unsupported request');
};
